SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

function main_local {
    :
}

function main_install {
    eval local $(root_env)
    [ -f "$pkg_map_path" ] || root_setup

    local src="$1"
    case "$src" in
    git@*.git) (
            local name="$(basename "$src" .git)"
            local path="$pkg_path/$name"

            git clone "$src" "$path" ||:

            if grep "^$name=" "$pkg_map_path" &>/dev/null; then
                sed -i "s%^$name=.*%$name=$path%" "$pkg_map_path"
            else
                printf '%s\n' "$name=$path" >> "$pkg_map_path"
            fi
            if grep "^${name}__master=" "$pkg_map_path" &>/dev/null; then
                sed -i "s%^${name}__master=.*%${name}__master=$path%" "$pkg_map_path"
            else
                printf '%s\n' "${name}__master=$path" >> "$pkg_map_path"
            fi
        );;
    esac
}

function main_dep {
    local src="$1" path= name=
    main_install "$src"

    case "$src" in
    git@*.git) name="$(basename "$src" .git)";;
    esac

    if path="$(wdir_resolve)"; then
        if grep "^$name=" "$path" &>/dev/null; then
            sed -i "s%^$name=.*%$name=${name}__master%" "$path"
        else
            printf '%s\n' "$name=${name}__master" >> "$path"
        fi
    fi
}

function wdir_resolve (
    while [ "$PWD" != "$HOME" -a "$PWD" != '/' ]; do
        if [ -f 'resolve.sh' ]; then
            printf '%s/resolve.sh\n' "$PWD"
            return 0
        fi
        cd ..
    done
    return 1
)
