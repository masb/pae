SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

function main_run {
    local prefix="$1"; shift
    if ! prefix="$(prefix_resolve "$prefix")"; then
        false # error forward
        return
    fi

    eval local $(root_env)
    local prefix_path="$prefix_path/$prefix"
    eval local $(cat "$recipe_path/$prefix")
    case "$prefix" in
    bash*) {
            run_pkg
            printf '%s' 'exec "$0"'
            printf ' %s' $(printf '%q\n' "$@")
        } | "$prefix_path/$target" -;;
    esac
}

function run_pkg {
    eval local $(root_env)
    if [ -f "$pkg_map_path" ]; then
        cat <<EOF
function pkg {
    [ -d "$pkg_path" ] || return '0'

    local path="\$1"
    local pkg="\$(dirname "\$path")"

    if grep "^\$pkg=" "$pkg_map_path" &>/dev/null; then
        eval local \$(cat "$pkg_map_path")
        pkg="\${!pkg}"
    fi

    printf '%q\n' "\$pkg/\$(cut -d/ -f2- <<< "\$path")"
}

export -f pkg
EOF
        return
    fi

    cat <<EOF
function pkg { :; }
export -f pkg
EOF
}
