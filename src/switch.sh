SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

function main_switch {
    local prefix="$1"
    if ! prefix="$(prefix_resolve "$prefix")"; then
        false # error forward
        return
    fi

    eval local $(root_env)
    local prefix_path="$prefix_path/$prefix"
    eval local $(cat "$recipe_path/$prefix")
    case "$src" in
    http*.deb)
            local path="$cache_path/$(basename "$src")"
            [ -f "$path" ] || wget -qO "$path" "$src"
            mkdir -p "$prefix_path"
            ar p "$path" data.tar.xz | {
                tar -C "$prefix_path" -xJf- $(tr ':' ' ' <<< "$target")
            }

            local lang=`cut -d- -f1 <<< "$prefix"`
            if grep "^$lang=" "$prefix_map_path" &>/dev/null; then
                sed -i "s%^$lang=.*%$lang=$prefix%" "$prefix_map_path"
            else
                printf '%s\n' "$lang=$prefix" >> "$prefix_map_path"
            fi
        ;;
    esac
}
