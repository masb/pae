SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "$SCRIPT_DIR/prefix.sh"
source "$SCRIPT_DIR/switch.sh"
source "$SCRIPT_DIR/run.sh"
source "$SCRIPT_DIR/install.sh"
source "$SCRIPT_DIR/snip.sh"

function main_usage {
    cat <<EOF
Usage: $PROGNAME <command> [args...]
Valid action commands:
    prefix [options] [<name>]
    switch <prefix>
    run <prefix> [<args>...]

    local [<pattern>]
    install <source>
    dep <source>

    snip <id> <name>            generate files from template ID using name
    help                        print this message and exit
    version                     print version informations and exit
Valid options for prefix:
    <name>
    -a, --available
    -l, --list
EOF
}

function main_version {
    cat <<EOF
$PROGNAME $VERSION
EOF
}

function main_cmd {
    local cmd="$1"; shift
    case "$cmd" in
    help) main_usage;;
    prefix|switch|run) "main_$cmd" "$@";;
    local|install|dep) "main_$cmd" "$@";;
    snip|version) "main_$cmd" "$@";;
    *) main_run "$cmd" "$@";;
    # *) main_run "$1" "$@" || main_usage >&2 && false;;
    esac
}
