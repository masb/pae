SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

# source "$(pkg bash/log.sh)"

function main_snip {
    local tmpl="$1" name="$2"
    local uname="$(tr '[:lower:]' '[:upper:]' <<< "$name")"

    eval local $(root_env)
    cat "$tmpl_path/$tmpl/sentinel.rc" | {
        while read i; do
            if [ "$(cut -d' ' -f1 <<< "$i")" = 'import' ]; then
                local subname="$name"
                if [ "$(cut -d' ' -f3 <<< "$i")" = '-n' ]; then
                    subname="$(cut -d' ' -f4 <<< "$i")"
                fi

                main_snip "$(cut -d' ' -f2 <<< "$i")" "$subname"
                continue
            fi
            o="$(sed "s/phname/$name/g; s/PHNAME/$uname/g" <<< "$i")"
            o="$(basename "$o")"
            printf '%s => %s\n' "$i" "$o"
            cpp -traditional -P -I "$tmpl_path" "$tmpl_path/$i" | tail +12 | {
                sed "s/phname/$name/g; s/PHNAME/$uname/g" | tee "$o" >/dev/null
            }
        done
    }
}
