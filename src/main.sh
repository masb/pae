SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

export PROGNAME='pae'
export VERSION='latest'

source "$SCRIPT_DIR/root.sh"
source "$SCRIPT_DIR/main_cmd.sh"

case "$-" in *i*);; *) main_cmd "$@";; esac
