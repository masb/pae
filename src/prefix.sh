SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

function main_prefix {
    [ "$#" = '0' ] && {
        prefix_current
        return
    }
    local arg="$1"
    case "$arg" in
    -l|--list) prefix_list "$arg";;
    -a|--available) prefix_available "$arg";;
    *) prefix_info "$arg";;
    esac
}

function prefix_current {
    eval local $(root_env)
    local prefix="$PAE_PREFIX"

    if [ -z "$prefix" -a -f "$prefix_map_path" ]; then
        prefix=$(
            cat "$prefix_map_path" | {
                cut -d= -f2- | tr '\n' ':' | head -c-1
            }
        )
    fi

    [ -z "$prefix" ] || printf '%s\n' "$prefix"
}

function prefix_list {
    eval local $(root_env)
    [ -f "$prefix_map_path" ] || return '0'

    cat "$prefix_map_path" | cut -d= -f1
}

function prefix_available {
    eval local $(root_env)
    [ -d "$recipe_path" ] || root_setup
    ls "$recipe_path" | cat
}

function prefix_info {
    local prefix="$1"
    if ! prefix="$(prefix_resolve "$prefix")"; then
        false # error forward
        return
    fi

    eval local $(root_env)
    cat "$recipe_path/$prefix"
}

function prefix_resolve {
    eval local $(root_env)
    [ -d "$recipe_path" ] || root_setup

    local prefix="$1"
    if [ -f "$recipe_path/$prefix" ]; then
        printf '%s\n' "$prefix"
        return
    fi

    if [ -f "$prefix_map_path" ]; then
        if grep "^$prefix=" "$prefix_map_path" &>/dev/null; then
            eval local $(cat "$prefix_map_path")
            prefix_resolve "${!prefix}"
            return
        fi
    fi

    false # error not found
}
