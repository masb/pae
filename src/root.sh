SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

# root layout:
#   .local/share/pae
#   |-- cache [todo: move to .cache/pae]
#   |-- remote [pae git repository]
#   |   |-- ... [unused git sub-directories]
#   |   |-- recipe
#   |   |   `-- ... [prefix recipes]
#   |   `-- tmpl
#   |       `-- ... [bootstrap templates]
#   |-- pkg
#   |   |-- resolve.sh [resolve map of package versions, self-managed]
#   |   `-- ... [installed packages]
#   `-- prefix
#       |-- resolve.sh [resolve map of prefix versions, self-managed]
#       `-- ... [installed prefixes]

function root_env {
    local root="$PAE_ROOT"
    if [ -z "$root" ]; then
        root="${XDG_DATA_HOME:-$HOME/.local/share}/$PROGNAME"
    fi
    printf 'root_path=%q\n' "$root"
    printf 'cache_path=%q\n' "$root/cache"

    local remote="$root/remote"
    printf 'remote_path=%q\n' "$remote"
    printf 'recipe_path=%q\n' "$remote/recipe"
    printf 'tmpl_path=%q\n' "$remote/tmpl"

    local pkg="$root/pkg"
    printf 'pkg_path=%q\n' "$pkg"
    printf 'pkg_map_path=%q\n' "$pkg/resolve.sh"

    local prefix="$root/prefix"
    printf 'prefix_path=%q\n' "$prefix"
    printf 'prefix_map_path=%q\n' "$prefix/resolve.sh"
}

function root_setup {
    eval local $(root_env)
    if [ ! -d "$root_path" ]; then
        mkdir -p "$root_path"
    fi
    if [ ! -d "$remote_path" ]; then
        git clone 'https://codeberg.org/masb/pae' "$remote_path"
    else
        git -C "$remote_path" pull origin master
    fi
    if [ ! -d "$cache_path" ]; then
        mkdir -p "$cache_path"
    fi
    if [ ! -f "$pkg_map_path" ]; then
        mkdir -p "$pkg_path"
        touch "$pkg_map_path"
    fi
    if [ ! -f "$prefix_map_path" ]; then
        mkdir -p "$prefix_path"
        touch "$prefix_map_path"
    fi
}
