TEST_CASE=

function not { "$@" && return 1 ||:; }

function run {
    local argv=`printf '%q ' "$@"`
    printf 'TEST_CASE="%s" ' "$argv"
    printf 'out=$(%s) ' "$argv 2>&1"
    printf 'err="$?" ||:\n'
}

function fatal {
    printf 'Test failed: %s: %s\n' "$TEST_CASE" "$*" && false
}

function test_err {
    [ -z "$2" -a "$1" = 0 ] || {
        [ "$1" = "$2" ] || fatal "unexpected error: $1"
    }
}
