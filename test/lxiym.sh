SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"
source "$SCRIPT_DIR/testing.sh"

function main {
    set -e

    # export PAE_ROOT="$XDG_RUNTIME_DIR/pae"
    # rm -rf "$PAE_ROOT"

    # # print current prefix name
    # #   never fail
    # #   print nothing if root corrupted
    # eval `run pae prefix`
    # test_err "$err" || fatal "$out"
    # [ -z "$out" ] || fatal "unexpected output: $out"

    # # list all intalled prefix
    # #   never fail
    # #   print nothing if root corrupted
    # #   print nothing if no prefix
    # eval `run pae prefix -l`
    # test_err "$err" || fatal "$out"
    # [ -z "$out" ] || fatal "unexpected output: $out"

    # # list all available prefix
    # #   never fail
    # #   clone prefix repo if does not exist
    # #   print nothing if no prefix
    # eval `run pae prefix -a`
    # test_err "$err" || fatal "$out"
    # [ -n "$out" ] || fatal "empty output"

    # # print prefix details
    # #   fail if prefix not found
    # eval `run pae prefix bash-5.1`
    # test_err "$err" || fatal "$out"
    # [ -n "$out" ] || fatal "empty output"

    # local expected="$out"

    # eval `run pae prefix bash`
    # test_err "$err" 1
    # [ -z "$out" ] || fatal "unexpected output: $out"
    # # [ "$out" = "$expected" ] || fatal "unexpected output: $out"

    # # set as current prefix
    # #   install if available
    # #   fail if not found
    # #   print new env
    # eval `run pae switch bash-5.1`
    # test_err "$err" || fatal "$out"

    # eval `run pae switch bash-4.4`
    # test_err "$err" || fatal "$out"

    # eval `run pae prefix`
    # test_err "$err" || fatal "$out"
    # [ "$out" = 'bash-4.4' ] || fatal "unexpected output: $out"

    # # run bash using default prefix
    # #   install if available
    # #   fail if not found
    # eval `run pae bash -c 'echo ok'`
    # test_err "$err" || fatal "$out"
    # [ "$out" = 'ok' ] || fatal "unexpected output: $out"

    # eval `run pae install 'git@codeberg.org:masb/dummy.git'`
    # test_err "$err" || fatal "$out"
    # [ -n "$out" ] || fatal "unexpected output: $out"

    # eval `run pae bash -c 'source $(pkg "dummy/ok.sh") && ok'`
    # test_err "$err" || fatal "$out"
    # [ "$out" = 'ok' ] || fatal "unexpected output: $out"

    # pae_dep 'git@codeberg.org:masb/dummy.git'

    pushd "$XDG_RUNTIME_DIR"
    eval `run pae snip bash/error foo`
    test_err "$err" || fatal "$out"
    ls foo_error.sh
    popd
    fatal "$out"

    printf '%s\n' "All tests passed successfully"
}

main
