#include "bash/file/phname.sh"

PHNAME_EARG=2

function phname_err {
    local err="${1:-1}"; shift

    printf 'phname error: ' >&2
    case "$err" in
    "$PHNAME_EARG") printf 'Invalid argument: %s\n' "$*" >&2;;
    *) printf 'Unknown error: code %s\n' "$err" >&2;;
    esac

    return "$err"
}
