#include "bash/file/phname.sh"

function phname_usage {
    cat <<EOF
Usage: $PROGNAME <command> [args...]
Valid action commands:
    help                print this message and exit
    version             print version informations and exit
EOF
}

function phname_version {
    cat <<EOF
$PROGNAME $VERSION
EOF
}

function phname_cmd {
    local cmd="$1"; shift
    case "$cmd" in
    help) phname_usage;;
    version) "phname_$cmd" "$@";;
    *) phname_usage >&2 && false;;
    esac
}
