SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

export PROGNAME='main'
export VERSION='latest'

source "$SCRIPT_DIR/main_cmd.sh"

case "$-" in *i*);; *) main_cmd "$@";; esac
