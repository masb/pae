#include "bash/file/phname.sh"
source "$SCRIPT_DIR/phname_error.sh"

function phname_dump {
    for i in name val; do
        i="this_$i"
        printf '%s=%q\n' "$i" "${!i}"
    done
}

function default_phname_opt {
    cat <<\EOF
val=42
EOF
}

function new_phname {
    local name="$1"
    [ -n "$name" ] || {
        phname_err "$PHNAME_EARG" missing name
        return
    }

    local opt="${2:-$(default_phname_opt)}"
    eval local $opt

    local this_name="$name"
    [ -n "${val+ }" ] && local this_val="$val"
    phname_dump
}
