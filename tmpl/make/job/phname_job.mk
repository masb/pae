## paths
CMD :=		sleep 42
CMDID !=	tr ' ' . <<< '$(CMD)'
OUTFILE :=	$(OUTDIR)/$(CMDID).out
PIDFILE :=	$(OUTDIR)/$(CMDID).pid
CTRLFILE :=	$(OUTDIR)/$(CMDID).lock

# ----- #

run:		kill $(OUTFILE)

kill:		$(wildcard $(PIDFILE))

.lock:;		touch $(CTRLFILE)

$(OUTFILE):	.lock $(CTRLFILE)
		printf ':: Starting process in background:\n
		printf '>>> %s\n' '$(CMD)'
		nohup $(CMD) &>$@ & echo $$! > $(PIDFILE)
		printf ':: Attach commands:\n'
		printf '>>> %s %q\n' 'less +F' $@
		printf '>>> %s %q\n' 'tail -f' $@

$(PIDFILE):	.lock $(CTRLFILE)
		printf ':: Killing process %s\n' "$$(cat $@)"
		kill -INT "$$(cat $@)" ||:
		rm -fv $@

.PHONY:		run kill
.SILENT:	.lock $(OUTFILE) $(PIDFILE)

# ----- #

distclean:	kill
TRASH +=	$(OUTFILE) $(PIDFILE) $(CTRLFILE)
