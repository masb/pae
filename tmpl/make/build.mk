CSRCDIR :=	$(shell dirname $(lastword $(MAKEFILE_LIST)))
COUTDIR :=	$(OUTDIR)/cobj

CSRC :=		$(wildcard $(CSRCDIR)/*.c)
COBJ :=		$(CSRC:$(CSRCDIR)/%.c=$(COUTDIR)/%.o)

BINFILE :=	$(OUTDIR)/$(PROGNAME)

# ----- #

all:		$(BINFILE)

$(BINFILE):	$(COBJ) | $(OUTDIR)
		gcc $^ -o $@
		@printf ":: Build complete: $@\n"

$(COUTDIR)/%.o:	$(CSRCDIR)/%.c | $(COUTDIR)
		gcc -c -o $@ $<

$(COUTDIR):;	mkdir -p $@

include job/phname_job.mk

# ----- #

TMPTRASH +=	$(COBJ)
TRASH +=	$(BINFILE) $(COUTDIR)
